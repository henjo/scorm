# Scorm runner

This repo makes it easy to run a scorm module locally on docker.

To run the scorm, download the zip, unzip it in the www folder and run `docker-compose up -d`.

You will want to run the scorm standalone, so point your browser to: http://localhost/standalone.html .
